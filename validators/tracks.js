const { check } = require("express-validator");
const validateResults = require("../utils/handleValidator")

const validatorCreateItem = [
    check("nombre")
    .exists()
    .notEmpty(),
    check("precio")
    .exists()
    .notEmpty(),
    check("modelo")
    .exists()
    .notEmpty(),
    check("marca")
    .exists()
    .notEmpty(),
    check("mediaId")
    .exists()
    .notEmpty(),
    (req, res, next) => {
        return validateResults(req, res, next)
    }
];
//.isLength({min:5, max:90})

const validatorGetItem = [
    check("id")
    .exists()
    .notEmpty(),
    (req, res, next) => {
        return validateResults(req, res, next)
    }
];

module.exports = { validatorCreateItem, validatorGetItem };