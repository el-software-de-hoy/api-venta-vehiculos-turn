const { matchedData } = require("express-validator")
const { tracksModel } = require("../models")
const { handleHttpError } = require("../utils/handleError")
    /** 
     *Obtener una lista
     *@param{*} req
     *@param{*} res
     */
const getItems = async(req, res) => {
    try {
        const user = req.user
        const data = await tracksModel.findAllData({});
        //const data = await tracksModel.find({});
        res.send({ data, user })
    } catch (e) {
        handleHttpError(res, "ERROR_GET_ITEMS");
    }
};
/** 
 *Obtener un detalle
 *@param{*} req
 *@param{*} res
 */
const getItem = async(req, res) => {
    try {
        req = matchedData(req);
        const { id } = req;
        const data = await tracksModel.findOneData(id);
        //const data = await tracksModel.findById(id);
        res.send({ data });
    } catch (e) {
        handleHttpError(res, "ERROR_GET_ITEM");
    }
};

const searchItem = async(req, res) => {
    //try {
        const dat = req.body;
        console.log(dat);
        const data = await tracksModel.find(dat);
        res.send({ data });
    /*} catch (e) {
        handleHttpError(res, "ERROR_GET_ITEM");
    }*/
};


/** 
 *insertar un registro
 *@param{*} req
 *@param{*} res
 */
const createItem = async(req, res) => {
    try {
        const body = matchedData(req)
        const data = await tracksModel.create(body)
        res.send({ success:1, datos: data });
    } catch (e) {
        res.send({ success:0, message: "No se pudo agregar automovil" });
    }
};
/** 
 *actualizar un registro
 *@param{*} req
 *@param{*} res
 */
 const updateItem = async (req, res) => {
    //try {
      const {id } = matchedData(req);
      const data = await tracksModel.updateOne({_id:id},{
        $set:{
            status: 0
        }
      });

      res.send({ success:1, datos: data });
    //} catch (e) {
        //res.send({ success:0, message: "Error al actualizar estatus vendido" });
       //handleHttpError(res, "ERROR_UPDATE_ITEMS");
    //}
  };

  const updateItem2 = async (req, res) => {
    //try {
      const {id } = matchedData(req);
      const data = await tracksModel.updateOne({_id:id},{
        $set:{
            status: 1
        }
      });
      res.send({ success:1, datos: data });
    //} catch (e) {
      //res.send({ success:0, message: "Error al actualizar estatus disponible" });
      //handleHttpError(res, "ERROR_UPDATE_ITEMS");
    //}
  };


/** 
 *eliminar un registro
 *@param{*} req
 *@param{*} res
 */
const deleteItem = async(req, res) => {
    try {
        req = matchedData(req);
        const { id } = req;
        // Borrado fisico de la base de datos
        // const data = await tracksModel.deleteOne({_id:id});

        // Borrado logico de la base de datos
        const data = await tracksModel.delete({ _id: id });
        
        res.send({ success:1, datos: data });
    } catch (e) {
        res.send({ success:0, message: "Error al eliminar automovil" });
        //handleHttpError(res, "ERROR_DELETE_ITEM");
    }
};

module.exports = { getItems, getItem, createItem, updateItem, deleteItem, searchItem, updateItem2 }