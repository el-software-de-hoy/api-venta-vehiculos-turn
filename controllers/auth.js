const { matchedData } = require("express-validator")
const { encrypt, compare } = require("../utils/handlePassword")
const { tokenSing } = require("../utils/handleJwt")
const { usersModel } = require("../models")
const { handleHttpError } = require("../utils/handleError")

/**
 * Este controlador es el encargado de registrar un usuario
 * @param {*} req 
 * @param {*} res 
 */
const registerCtrl = async(req, res) => {
    try {
        req = matchedData(req);
        const password = await encrypt(req.password);
        const body = {...req, password }
        const dataUser = await usersModel.create(body)
        dataUser.set('password', undefined, { strict: false })

        const data = {
            token: await tokenSing(dataUser),
            user: dataUser
        }

        res.send({ data });
    } catch (e) {
        handleHttpError(res, "ERROR_REGISTER_USER");
    }
}

/**
 * Este controlador es el encargado de logear una persona
 * @param {*} req 
 * @param {*} res 
 */
const loginCtrl = async(req, res) => {

    try {
        req = matchedData(req);
        const user = await usersModel.findOne({ email: req.email })
            .select('password name role email type');  //comentar para usar con mysql descomentar parta USAR PARA MONGO
        if (!user) {
            res.send({ success: 0, message: "El usuario no existe" });
            //handleHttpError(res, "USER_NO_EXIST");
            return
        }

        const hashPassword = user.get('password');

        const check = await compare(req.password, hashPassword);

        if (!check) {
            res.send({ success: 0, message: "Contraseña incorrecta" });
            //handleHttpError(res, "PASSWORD_INVALID", 401);
            return
        }

        user.set('password', undefined, { strict: false })
        const data = {
            token: await tokenSing(user),
            user
        }

        res.send({ success: 1, datos: data });
    } catch (e) {
        res.send({ success: 0, message: "Error en el login de usuario" });
        //handleHttpError(res, "ERROR_LOGIN_USER");
    }

}

module.exports = { registerCtrl, loginCtrl };