const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");
const TracksScheme = new mongoose.Schema({
    nombre: {
        type: String,
    },
    precio: {
        type: String,
    },
    modelo: {
      type: String,
    },
    marca: {
      type: String,
    },
    status: {
      type: Number,
      default: 1,
    },
    mediaId: {
        type: mongoose.Types.ObjectId,
    },
}, {
    timestamps: true,
    versionKey: false,
})

/**
 * Implementar Metodo propio con relacion al storage
 */

 TracksScheme.statics.findAllData = function () {
    const joinData = this.aggregate([
      //TODO Tracks
      {
        $lookup: {
          from: "storages", //TODO Tracks --> storages
          localField: "mediaId", //TODO Tracks.mediaId
          foreignField: "_id", //TODO Straoges._id
          as: "image", //TODO Alias!
        },
      },
      {
        $unwind: "$image",
      }
    ]);
    return joinData;
  };

  TracksScheme.statics.findOneData = function (id) {
    const joinData = this.aggregate([
      {
        $match: {
          _id: mongoose.Types.ObjectId(id),
        },
      },
      {
        $lookup: {
          from: "storages", //TODO Tracks --> storages
          localField: "mediaId", //TODO Tracks.mediaId
          foreignField: "_id", //TODO Straoges._id
          as: "image", //TODO Alias!
        },
      },
      {
        $unwind: "$image",
      }
    ]);
    return joinData;
  };

TracksScheme.plugin(mongooseDelete, { overrideMethods: 'all' });
module.exports = mongoose.model("tracks", TracksScheme)