const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");

const UserScheme = new mongoose.Schema({
    name: {
        type: String,
    },
    email: {
        type: String,
        unique: true,
    },
    password: {
        type: String,
        select: false,
    },
    role: {
        type: ["user", "admin"],
        default: "user",
    },
    type: {
        type: ["0", "1"],
        default: "1",
    },
}, {
    timestamps: true,
    versionKey: false,
})

UserScheme.plugin(mongooseDelete, { overrideMethods: 'all' });
module.exports = mongoose.model("users", UserScheme)