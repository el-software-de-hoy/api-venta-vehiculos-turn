const { sequelize } = require("../../config/mysql");
const DataTypes = require("sequelize");

const User = sequelize.define( 
    "users",
    {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        email: {
            type: DataTypes.STRING,
        },
        password: {
            type: DataTypes.STRING,
        },
        role: {
            type: DataTypes.ENUM(["user", "admin"]),
        }, 
        type: {
            type: DataTypes.ENUM(["0", "1"]),
        }, 
    },
    {
        timestamps: true,
    }
);

module.exports = User;