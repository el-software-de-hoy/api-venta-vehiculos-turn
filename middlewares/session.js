const { handleHttpError } = require("../utils/handleError")
const { verifyToken } = require("../utils/handleJwt")
const { usersModel } = require("../models")
const getProperties = require("../utils/handlePropertiesEngine");
const propertiesKey = getProperties();

const authMiddleware = async(req, res, next) => {

    try {

        if (!req.headers.authorization) {
            handleHttpError(res, "NEED_SESSION", 403);
            return
        }

        const token = req.headers.authorization.split(' ').pop(); //TODO Bearer
        const dataToken = await verifyToken(token);

        if(!dataToken){
            //handleHttpError(res, "NOT_PAYLOAD_DATA", 401);
            res.send({ success:0, message: "La sesion a expirado" });
            return
        }
        const query = {
            [propertiesKey.id]:dataToken[propertiesKey.id]
        }

        //dependiendo si es relacional o no hace busqueda por _id o id
        const user = await usersModel.findOne({query})
        req.user = user
        next()

    } catch (e) {
        handleHttpError(res, "NOT_SESSION", 401);
    }

}

module.exports = authMiddleware