// const users = require("../models");
const { handleHttpError } = require("../utils/handleError")

/**
 * Array con los roles permitidos
 * @param {*} rol 
 * @returns 
 */
const checkRol = (roles) => (req, res, next) => {
    try {
        const { user } = req;
        console.log(user);
        
        const rolesByUser = user.role;
        // ["admin", "manager"] 
        const checkValueRol = roles.some((rolSingle) => rolesByUser.includes(rolSingle)) //true or false
        if (!checkValueRol) {
            res.send({ success:0, message: "El usuario no tiene permisos para esta acción" });
            //handleHttpError(res, "USER_NO_PERMISSIONS", 403);
            return
        }

        next()
    } catch (e) {
        handleHttpError(res, "ERROR_PERMISSIONS", 403);
    }
}

module.exports = checkRol;