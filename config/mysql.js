const { Sequelize } = require("sequelize");
const database = process.env.MYQSL_DATABASE;
const username = process.env.MYQSL_USER;
const password = process.env.MYQSL_PASSWORD;
const host = process.env.MYQSL_HOST;

const sequelize = new Sequelize(
    database,
    username,
    password, {
        host,
        dialect: "mysql"
    }
)

const dbConnectMySql = async() => {
    try {
        await sequelize.authenticate();
        console.log('MYSQL CONEXION CORRECTA')
    } catch (error) {
        console.log('MYSQL ERROR DE CONEXION', error)
    }
}

module.exports = { sequelize, dbConnectMySql }