require("dotenv").config() //incorporar variables de entorno
const express = require("express") //declarado el paquede de express para levantar el servicio we
const cors = require("cors") //le decimos a la app que se crea que haha uso de cors
const dbConnectNoSql = require('./config/mongo') //importar database
const { dbConnectMySql } = require('./config/mysql')
const app = express() //
app.use(cors()) //hacer uso de cors
app.use(express.json()) //Preparar para recibir json
app.use(express.static("storage")) //obtener ruta de archivos de storage
const port = process.env.PORT || 3000; //constante con el puerto por ambiente si no lo encuentra el 3000.
const ENGINE_DB = process.env.ENGINE_DB;


//aqui invocamos a las rutas
//personas entren a http://localhost/api/___
app.use("/api", require("./routes"))

    app.listen(port, () => {
        console.log('tu app esta corriendo por http://localhost:' + port);
    })

    if(ENGINE_DB == 'nosql'){
        console.log('soy nosql');
        dbConnectNoSql()
    }else{
        console.log('soy mysql');
        dbConnectMySql()
    }