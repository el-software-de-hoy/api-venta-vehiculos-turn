const express = require("express");
const router = express.Router();
const authMiddleware = require("../middlewares/session")
const checkRol = require("../middlewares/rol")
const { validatorCreateItem, validatorGetItem } = require("../validators/tracks")
const { getItems, getItem, createItem, updateItem, deleteItem, searchItem, updateItem2 } = require("../controllers/tracks")


/**
 * Lista los items
 */

router.get("/", getItems)

/**
 * Obtener un detalle
 */
//multiples variables /:id/:var1/:var2
router.get("/:id", validatorGetItem, getItem)

/**
 * Crear un registro
 */
//customHeader,

router.post("/", authMiddleware, checkRol(["user", "admin"]), validatorCreateItem, createItem)

router.post("/search", searchItem)

/**
 * Actualizar un registro
 */

router.put("/:id", authMiddleware, validatorGetItem, updateItem)

router.put("/status/:id", authMiddleware, validatorGetItem, updateItem2)

/**
 * Eliminar 
 */

router.delete("/:id", authMiddleware, checkRol(["user", "admin"]), validatorGetItem, deleteItem)

module.exports = router