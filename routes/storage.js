const express = require("express");
const router = express.Router();
const uploadMiddleware = require("../utils/handleStorage");
const { validatorGetItem } = require("../validators/storage")
const { getItems, getItem, createItem, deleteItem } = require("../controllers/storage");

/**
 * Lista los items
 */
router.get("/", getItems)

/**
 * Obtener un detalle
 */
//multiples variables /:id/:var1/:var2
// validatorGetItem, 
router.get("/:id", validatorGetItem, getItem)
    /**
     * Crear un registro
     */
    //.multi varios archivos
router.post("/", uploadMiddleware.single("myfile"), createItem);
/**
 * Eliminar 
 */
router.delete("/:id", validatorGetItem, deleteItem)

module.exports = router