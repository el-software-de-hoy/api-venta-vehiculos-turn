const express = require("express");
const { registerCtrl, loginCtrl } = require("../controllers/auth");
const router = express.Router();
const { validatorRegister, validatorLogin } = require("../validators/auth")

//TODO http://localhost:3000/api/login
//TODO http://localhost:3000/api/register

router.post("/register", validatorRegister, registerCtrl);
router.post("/login", validatorLogin, loginCtrl);


module.exports = router;